/* Copyright (C) 2021. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_NPU_HDC_H
#define TENSORFLOW_NPU_HDC_H

#include <utility>

#include "acl/acl_tdt.h"
#include "tensorflow/core/framework/tensor.h"

#include "npu_micros.h"

class HdcChannel {
 public:
  static tensorflow::Status Create(uint32_t device_id, const std::string &name,
                                   std::shared_ptr<HdcChannel> *guarded_channel);

  ~HdcChannel();

  tensorflow::Status SendTensors(const std::vector<tensorflow::Tensor> &tensors);

  tensorflow::Status NotifyFinish();

  tensorflow::Status NotifyAbnormal();

  void Destroy();

 private:
  HdcChannel(uint32_t device_id, std::string name);
  tensorflow::Status Init();
  acltdtChannelHandle *handle_;
  int32_t device_id_;
  std::string name_;
  std::atomic_bool destroyed_{false};
};

#endif  // TENSORFLOW_NPU_HDC_H
